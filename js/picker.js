Drupal.behaviors.placemark = {
  attach: function(context) {
    if (YMaps != null) {
      jQuery("#edit-synapse-geoposition").after("<div id='map-placemark' style='height:300px'></div>");
      // �������� ���������� ����� � ��� �������� � ���������� ����������
      var placemark_map = new YMaps.Map(YMaps.jQuery("#map-placemark")[0]);

      // ��������� ��� ����� �� ������ � ��������
      var coordX = Drupal.settings.synapse.latitude;
      var coordY = Drupal.settings.synapse.longitude;
      var Zoom   = Drupal.settings.synapse.longitude;
      placemark_map.setCenter(new YMaps.GeoPoint(coordX,coordY), Zoom);

      // ��������� ���������
      placemark_map.addControl(new YMaps.Zoom());
      placemark_map.addControl(new YMaps.TypeControl([
            YMaps.MapType.MAP,
            YMaps.MapType.SATELLITE,
            YMaps.MapType.HYBRID,
            YMaps.MapType.PMAP
        ], [0, 1, 2, 3]));

      // �������� �����
      var placemark = new YMaps.Placemark(new YMaps.GeoPoint(coordX, coordY), {draggable: true});
      placemark_map.addOverlay(placemark);


      // ��������� �������
      YMaps.Events.observe(placemark, placemark.Events.PositionChange, function (control, point) {
        YMaps.jQuery('#edit-map-latitude').
          attr('value', point.newPoint.getX());
        YMaps.jQuery('#edit-map-longitude').
          attr('value', point.newPoint.getY());
        YMaps.jQuery('#edit-map-zoom').
          attr('value', placemark_map.getZoom());
      });

      YMaps.Events.observe(placemark_map, placemark_map.Events.Click, function (placemark_map, point) {
        placemark.setGeoPoint(point.getGeoPoint());
      });

    }
  }
};
