(function ($) {

  Drupal.behaviors.todo = {

    attach: function(context, settings) {

      $('div.edit a', 'div.todo-item').each(function () {
        var button = $(this);
        button.click(function(e) {
          e.preventDefault();
          var href = $(this).attr('href');
          var reg = /^\/node\/([0-9]{1,10})\/edit$/;
          var res = reg.exec(href);
          if (res.length > 0) {
            var nid = res[1];
            alert( nid );
          }
        });
      });

    }

  };

})(jQuery);
