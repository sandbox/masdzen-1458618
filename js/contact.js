Drupal.behaviors.placemark = {
  attach: function(context) {
    if (YMaps != null) {
      jQuery(".node-full").after("<div id='map-placemark' style='height:300px'></div>");
      // �������� ���������� ����� � ��� �������� � ���������� ����������
      var placemark_map = new YMaps.Map(YMaps.jQuery("#map-placemark")[0]);

      // ��������� ��� ����� �� ������ � ��������
      var coordX = Drupal.settings.synapse.latitude;
      var coordY = Drupal.settings.synapse.longitude;
      var Zoom   = Drupal.settings.synapse.longitude;
      placemark_map.setCenter(new YMaps.GeoPoint(coordX,coordY), Zoom);

      // ��������� ���������
      placemark_map.addControl(new YMaps.Zoom());
      placemark_map.addControl(new YMaps.TypeControl([
            YMaps.MapType.MAP,
            YMaps.MapType.SATELLITE,
            YMaps.MapType.HYBRID,
            YMaps.MapType.PMAP
        ], [0, 1, 2, 3]));

      // �������� �����
      var placemark = new YMaps.Placemark(new YMaps.GeoPoint(coordX, coordY));
      placemark_map.addOverlay(placemark);

    }
  }
};
